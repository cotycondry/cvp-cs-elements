package com.cisco.cxdemo.cvp.cs;

import java.util.List;

import com.audium.server.action.contextservice.ContextService;
import com.audium.server.session.ActionElementData;
import com.audium.server.voiceElement.ActionElementBase;
import com.audium.server.voiceElement.AudiumElement;
import com.audium.server.voiceElement.ElementData;
import com.audium.server.voiceElement.ElementException;
import com.audium.server.voiceElement.Setting;
import com.cisco.cxdemo.cvp.cs.util.CxdUtil;
import com.cisco.thunderhead.client.ContextServiceClient;
import com.cisco.thunderhead.client.Operation;
import com.cisco.thunderhead.client.SearchParameters;
import com.cisco.thunderhead.customer.Customer;
import com.cisco.thunderhead.errors.ApiException;

public class CxdCustomerLookUpAction extends ActionElementBase implements AudiumElement, ContextService {
	private static final String TIMEOUT_RETRIES_TOOLTIP = "The number of times to retry the request when Context Service returns a timeout error in ApiException. Defaults to 1 retry.";
	private static final String TIMEOUT_RETRIES_DISPLAY_NAME = "Timeout Retries";
	private static final String TIMEOUT_RETRIES = "timeoutRetries";

	public String getElementName() {
		return "CXD_Customer_Lookup";
	}

	public String getDisplayFolderName() {
		return "Cisco\\CXDemo\\Context Service";
	}

	public String getDescription() {
		return "This action element invokes the Context Service for searching customers based on the query parameters. The response will be stored as element data.";
	}

	public Setting[] getSettings() throws ElementException {
		Setting[] settingArray = new Setting[3];

		settingArray[0] = new Setting("QueryParameters", "Query Parameters", "Mandatory field to insert query parameters.", true, true, true, 7);

		settingArray[1] = new Setting("Operator", "Operator", "Optional field to select operations like AND or OR to be performed on the query parameters.", false, true, false, new String[] {
				"AND", 
				"OR" 
		});
		settingArray[1].setDefaultValue("AND");
		// timeout retries - Integer input
		settingArray[2] = new Setting(TIMEOUT_RETRIES, TIMEOUT_RETRIES_DISPLAY_NAME, TIMEOUT_RETRIES_TOOLTIP, true, true, true, Setting.INT);
		settingArray[2].setDefaultValue("1");
		return settingArray;
	}

	public ElementData[] getElementData() throws ElementException {
		return new ElementData[] { 
				new ElementData("customer_count", "The number of Context Service customers returned by the query."),
				new ElementData("customer_id", "The Context Service customer ID for the first customer retrieved by the query."),
				new ElementData("customer_ref_url", "The Context Service reference URL for this customer."),
				new ElementData("fieldsets", "A comma-separated list of the Context Service fieldsets this customer has.")
		};
	}

	public void doAction(String name, ActionElementData actionData) throws Exception {
		// get the setting values for this element
		String operator = actionData.getActionElementConfig().getSettingValue("Operator", actionData);
		String queryParameters = actionData.getActionElementConfig().getSettingValue("QueryParameters", actionData);
		int timeoutRetries = actionData.getActionElementConfig().getIntSettingValue(TIMEOUT_RETRIES, actionData);

		actionData.addToLog("Query Parameters:", queryParameters);
		// get context service client
		ContextServiceClient contextServiceClient = CxdUtil.getContextServiceClient();

		// extract query map from the element settings value
		SearchParameters queryMap = CxdUtil.extractSearchParameters(queryParameters);
		

		List<Customer> list = null;
		// retry on timeout errors
		for (int i = 0; i <= timeoutRetries; i++) {
			try {
				// start the action length timer
				long beforeTime = System.currentTimeMillis();
				// perform action
				list = contextServiceClient.search(Customer.class, queryMap, Operation.valueOf(operator));
				// stop timer
				long afterTime = System.currentTimeMillis();
				// calculate timer
				Integer time = Integer.valueOf((int)(afterTime - beforeTime));
				// log timer
				actionData.addToLog("CS Customer Lookup", "Time taken: " + time.toString());
				
				// set the data on this element
				CxdUtil.populateElementData(actionData, list);
				// exit retry loop because we're done
				break;
			} catch (ApiException e) {
				if (e.getMessage().contains("timeout")) {
					actionData.addToLog("CS Customer Lookup", "failed due to timeout");
					// retry on timeout
					continue;
				} else {
					// rethrow other errors
					throw e;
				}
			}
		}
	}
}
