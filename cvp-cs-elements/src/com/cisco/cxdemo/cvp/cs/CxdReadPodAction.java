package com.cisco.cxdemo.cvp.cs;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;

import com.audium.server.action.contextservice.ContextService;
import com.audium.server.session.ActionElementData;
import com.audium.server.voiceElement.ActionElementBase;
import com.audium.server.voiceElement.AudiumElement;
import com.audium.server.voiceElement.ElementData;
import com.audium.server.voiceElement.ElementException;
import com.audium.server.voiceElement.Setting;
import com.cisco.cxdemo.cvp.cs.util.CxdUtil;
import com.cisco.thunderhead.client.ContextServiceClient;
import com.cisco.thunderhead.client.Operation;
import com.cisco.thunderhead.client.SearchParameters;
import com.cisco.thunderhead.errors.ApiException;
import com.cisco.thunderhead.pod.Pod;

public class CxdReadPodAction extends ActionElementBase implements AudiumElement, ContextService {
	private static final String TIMEOUT_RETRIES_TOOLTIP = "The number of times to retry the request when Context Service returns a timeout error in ApiException. Defaults to 1 retry.";
	private static final String TIMEOUT_RETRIES_DISPLAY_NAME = "Timeout Retries";
	private static final String TIMEOUT_RETRIES = "timeoutRetries";

	public String getElementName() {
		return "CXD_POD_Read";
	}

	public String getDisplayFolderName() {
		return "Cisco\\CXDemo\\Context Service";
	}

	public String getDescription() {
		return "This action element retrieves a list of PODs from a given Customer ID, and sets element data from the pod in the list at the specified index (starting with 0, being the most recent POD listed for this customer).";
	}

	public Setting[] getSettings() throws ElementException {
		Setting[] settingArray = new Setting[3];
		// customerId text input
		settingArray[0] = new Setting("customerId", "Customer ID", "Mandatory - the Customer ID you want to look up pods for.", true, true, true, Setting.STRING);
		// index - Integer input
		settingArray[1] = new Setting("index", "POD List Index", "The numerical index of the POD data to be set as element data. Default is 0, the first in the list, which is chronologically the latest POD created.", true, true, true, Setting.INT);
		settingArray[1].setDefaultValue("0");
		// timeout retries - Integer input
		settingArray[2] = new Setting(TIMEOUT_RETRIES, TIMEOUT_RETRIES_DISPLAY_NAME, TIMEOUT_RETRIES_TOOLTIP, true, true, true, Setting.INT);
		settingArray[2].setDefaultValue("1");
		return settingArray;
	}

	public ElementData[] getElementData() throws ElementException {
		return new ElementData[] { 
				new ElementData("pod_id", "The Context Service UUID for the new POD."),
				new ElementData("pod_ref_url", "The Context Service reference URL for this POD."),
				new ElementData("customer_id", "The Context Service UUID of the customer that the new POD was added to."),
				new ElementData("customer_ref_url", "The Context Service reference URL of the customer that the new POD was added to."),
				new ElementData("response_json", "The full JSON response string with all PODs found for this customer."),
				new ElementData("media_type", "The media type of the new POD."),
				new ElementData("pod_count", "Number of PODs returned for this customer.")
		};
	}

	public void doAction(String name, ActionElementData actionData) throws Exception {
		// get the setting values for this element
		String customerId = actionData.getActionElementConfig().getSettingValue("customerId", actionData);
		int index = actionData.getActionElementConfig().getIntSettingValue("index", actionData);
		int timeoutRetries = actionData.getActionElementConfig().getIntSettingValue(TIMEOUT_RETRIES, actionData);

		// get context service client
		ContextServiceClient contextServiceClient = CxdUtil.getContextServiceClient();

		// prepare search parameters - search using customer ID 
		SearchParameters searchParameters = new SearchParameters();
		searchParameters.add("customerId", customerId);

		Pod pod = null;
		// retry on timeout errors
		for (int i = 0; i <= timeoutRetries; i++) {
			try {
				// start the action length timer
				long beforeTime = System.currentTimeMillis();
				// perform action
				List<Pod> pods = contextServiceClient.search(Pod.class, searchParameters, Operation.AND);
				// stop timer
				long afterTime = System.currentTimeMillis();
				// calculate timer
				Integer time = Integer.valueOf((int)(afterTime - beforeTime));

				// log success
				actionData.addToLog("Search CS POD - status", "success");
				// log timer
				actionData.addToLog("Search CS POD - time taken", time.toString());
				// set the data on this element
				//		CxdUtil.setElementData(actionData, "status_code", create.getStatus());
				CxdUtil.setElementData(actionData, "status", "success");
				
				// select the pod as specified by the element config
				pod = pods.get(index);
				// populate this element data with all the POD data
				CxdUtil.setElementData(actionData, "pod_count", String.valueOf(pods.size()));
				try {
					CxdUtil.setElementData(actionData, "response_json", new JSONArray(pods).toString());
				} catch (JSONException e) {
					// continue
				}
				CxdUtil.populateElementData(actionData, pod);
				// exit retry loop because we're done
				break;
			} catch (ApiException e) {
				if (e.getMessage().contains("timeout")) {
					// retry on timeout
					continue;
				} else {
					// rethrow other errors
					throw e;
				}
			}
		}
	}
}
