package com.cisco.cxdemo.cvp.cs;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import com.audium.server.action.contextservice.ContextService;
import com.audium.server.session.ActionElementData;
import com.audium.server.voiceElement.ActionElementBase;
import com.audium.server.voiceElement.AudiumElement;
import com.audium.server.voiceElement.ElementData;
import com.audium.server.voiceElement.ElementException;
import com.audium.server.voiceElement.Setting;
import com.cisco.cxdemo.cvp.cs.util.CxdUtil;
import com.cisco.thunderhead.client.ClientResponse;
import com.cisco.thunderhead.client.ContextServiceClient;
import com.cisco.thunderhead.errors.ApiException;
import com.cisco.thunderhead.pod.Pod;
import com.cisco.thunderhead.tag.Tag;
import com.cisco.thunderhead.util.DataElementUtils;

public class CxdAddPodAction extends ActionElementBase implements AudiumElement, ContextService {
	private static final String ACTION = "Create CS POD";
	private static final String MEDIA_TYPE = "mediaType";
	private static final String MEDIA_TYPE_DISPLAY_NAME = "Media Type";
	private static final String MEDIA_TYPE_TOOLTIP = "Required - The new media type for the POD.";

	private static final String TIMEOUT_RETRIES_TOOLTIP = "The number of times to retry the request when Context Service returns a timeout error in ApiException. Defaults to 1 retry.";
	private static final String TIMEOUT_RETRIES_DISPLAY_NAME = "Timeout Retries";
	private static final String TIMEOUT_RETRIES = "timeoutRetries";

	public String getElementName() {
		return "CXD_POD_Add";
	}

	public String getDisplayFolderName() {
		return "Cisco\\CXDemo\\Context Service";
	}

	public String getDescription() {
		return "This action element creates a new POD in Context Service. The response will be stored as element data.";
	}

	public Setting[] getSettings() throws ElementException {
		Setting[] settingArray = new Setting[7];
		// customerId text input
		settingArray[0] = new Setting("customerId", "Customer ID", "Optional - the Context Service Customer UUID to assign this new POD to. If not specified, a new Customer ID will be created by Context Service automatically.", false, true, true, 3);

		settingArray[1] = new Setting("requestId", "Request ID", "Optional - the Context Service Request UUID to assign this new POD to.", false, true, true, 3);
		
		// mediaType - dropdown selection
		settingArray[2] = new Setting(MEDIA_TYPE, MEDIA_TYPE_DISPLAY_NAME, MEDIA_TYPE_TOOLTIP, true, true, false, new String[] {
				"voice", 
				"video", 
				"chat", 
				"email", 
				"social", 
				"mobile", 
				"web", 
				"alert", 
		});
		settingArray[2].setDefaultValue("voice");
		// tags - text input
		settingArray[3] = new Setting("tags", "Tags", "Optional - A comma-separated list of tags to be associated with the POD.", false, true, true, 3);
		// fieldsets - text input
		settingArray[4] = new Setting("fieldsets", "Fieldsets", "Required - A comma-separated list of fieldsets for this POD.", true, true, true, 3);
		settingArray[4].setDefaultValue("cisco.base.pod");
		// dataElements - key-value pairs
		settingArray[5] = new Setting("dataElements", "Data Elements", "Optional - additional data elements to attach data to the POD. All fields must belong to the cisco.base.pod fieldset or one of the fieldsets specified in the fieldsets setting on this element.", true, true, true, 7);
		// timeout retries - Integer input
		settingArray[6] = new Setting(TIMEOUT_RETRIES, TIMEOUT_RETRIES_DISPLAY_NAME, TIMEOUT_RETRIES_TOOLTIP, true, true, true, Setting.INT);
		settingArray[6].setDefaultValue("1");
		return settingArray;
	}

	public ElementData[] getElementData() throws ElementException {
		return new ElementData[] { 
				new ElementData("pod_id", "The UUID of the updated POD."),
				new ElementData("pod_ref_url", "The Context Service reference URL for this POD."),
				new ElementData("customer_id", "The UUID of the updated POD's customer."),
				new ElementData("customer_ref_url", "The Context Service reference URL for this POD's customer."),
				new ElementData("request_id", "The UUID of the Context Service Request that this POD belongs to."),
				new ElementData("media_type", "The media type of the POD."),
				new ElementData("status", "Whether the Context Service HTTP Add POD call succeeded or failed. Returns 'success' or 'failed'"),
				new ElementData("status_code", "The HTTP status code returned by Context Service Add POD call."),
				new ElementData("fieldsets", "A comma-separated list of the Context Service fieldsets this POD uses."),
				new ElementData("tags", "A comma-separated list of the Context Service tags this POD has.")
		};
	}

	public void doAction(String name, ActionElementData actionData) throws Exception {
		// get the setting values for this element
		String customerId = actionData.getActionElementConfig().getSettingValue("customerId", actionData);
		String requestId = actionData.getActionElementConfig().getSettingValue("requestId", actionData);
		String tagsString = actionData.getActionElementConfig().getSettingValue("tags", actionData);
		String fieldsetsString = actionData.getActionElementConfig().getSettingValue("fieldsets", actionData);
		String mediaType = actionData.getActionElementConfig().getSettingValue("mediaType", actionData);
		String dataElementsString = actionData.getActionElementConfig().getSettingValue("dataElements", actionData);
		int timeoutRetries = actionData.getActionElementConfig().getIntSettingValue(TIMEOUT_RETRIES, actionData);

		// get context service client
		ContextServiceClient contextServiceClient = CxdUtil.getContextServiceClient();

		// extract map from the element settings value
		Map<String, Object> dataElements = CxdUtil.extracteDataElements(dataElementsString);

		List<String> fieldsets = CxdUtil.extractFieldSetList(fieldsetsString);
		

		// prepare POD object for sending to Context Service
		// create a POD bean with data elements
		Pod pod = new Pod(DataElementUtils.convertDataMapToSet(dataElements));

		pod.setCustomerId(UUID.fromString(customerId));
		
		pod.setMediaType(mediaType);
		
		if (!tagsString.trim().isEmpty()) {
			Set<Tag> tags = CxdUtil.extractTagList(tagsString);
			pod.setTags(tags);
		}
		
		pod.setFieldsets(fieldsets);
		
		if (!requestId.trim().isEmpty()) {
			pod.setRequestId(UUID.fromString(requestId));
		}

		ClientResponse response = null;
		// retry on timeout errors
		for (int i = 0; i <= timeoutRetries; i++) {
			try {
				// start the action length timer
				long beforeTime = System.currentTimeMillis();
				// perform action
				response = contextServiceClient.create(pod);
				// stop timer
				long afterTime = System.currentTimeMillis();
				// calculate timer
				Integer time = Integer.valueOf((int)(afterTime - beforeTime));
				if (response.getStatus() >= 200 && response.getStatus() < 300) {
					// log success
					actionData.addToLog(ACTION + " - status", "success");
					// log timer
					actionData.addToLog(ACTION + " - time taken", time.toString());
					// set the data on this element
					CxdUtil.setElementData(actionData, "status_code", response.getStatus());
					CxdUtil.setElementData(actionData, "status", "success");
					CxdUtil.populateElementData(actionData, pod);
				} else {
					// log failure
					actionData.addToLog(ACTION + " - status", "failed");
					// log timer
					actionData.addToLog(ACTION + " - time taken", time.toString());
					// set the data on this element
					CxdUtil.setElementData(actionData, "status_code", response.getStatus());
					CxdUtil.setElementData(actionData, "status", "failed");
					// CxdUtil.populateElementData(actionData, pod);
				}
				// exit retry loop because we're done
				break;
			} catch (ApiException e) {
				if (e.getMessage().contains("timeout")) {
					// retry on timeout
					continue;
				} else {
					// rethrow other errors
					throw e;
				}
			}
		}
	}
}
