package com.cisco.cxdemo.cvp.cs.util;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import com.audium.core.serviceability.SvcMgrUtil;
import com.audium.server.AudiumException;
import com.audium.server.action.rest.client.NameValueTableUtil;
import com.audium.server.management.ContextServiceClientManager;
import com.audium.server.session.ActionElementData;
import com.audium.server.voiceElement.ElementException;
import com.cisco.thunderhead.BaseDbBean;
import com.cisco.thunderhead.Contributor;
import com.cisco.thunderhead.DataElement;
import com.cisco.thunderhead.client.ClientResponse;
import com.cisco.thunderhead.client.ContextServiceClient;
import com.cisco.thunderhead.client.SearchParameters;
import com.cisco.thunderhead.customer.Customer;
import com.cisco.thunderhead.errors.ApiException;
import com.cisco.thunderhead.pod.Pod;
import com.cisco.thunderhead.tag.Tag;

public class CxdUtil {

	public static Contributor getMachineContributor(ActionElementData actionData) {
		Contributor contributor = new Contributor("machine", getHostname(actionData));
		if (traceEnabled()) {
			trace("MESSAGE", "Contributor :" + contributor.toString());
		}
		return contributor;
	}

	public static String getHostname(ActionElementData actionData) {
		String contributorHostname;
		try {
			contributorHostname = InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
			actionData.logWarning("Unable to get contributor hostname. It has been set as Unknown.");
			contributorHostname = "Unknown";
		}
		return contributorHostname;
	}

	public static BaseDbBean<?> csCreate(ContextServiceClient contextServiceClient, BaseDbBean<?> b) throws AudiumException {
		ClientResponse clientResponse = null;
		try {
			clientResponse = contextServiceClient.create(b);
		} catch (ApiException e) {
			throw new AudiumException(e);
		}
		return processCsResponse(contextServiceClient, b, clientResponse);
	}

	private static BaseDbBean<?> processCsResponse(ContextServiceClient contextServiceClient, BaseDbBean<?> b,
			ClientResponse clientResponse) throws AudiumException {
		int status = clientResponse.getStatus();
		if (status >= 200 && status < 300) {
			// retrieve the new POD and return it
			return csGet(contextServiceClient, b);
		} else {
			throw new AudiumException("Context Service returned status code of " + status);
		}
	}

	public static BaseDbBean<?> csGet(ContextServiceClient contextServiceClient, BaseDbBean<?> b) throws AudiumException {
		try {
			return contextServiceClient.get(b.getClass(), b.getId().toString());
		} catch (Exception e) {
			throw new AudiumException(e);
		}
	}

	/**
	 * Sets the element data named "tags" to a comma-separated list of tag names
	 * @param actionData the CVP actionData for the element
	 * @param pod the Context Service POD
	 * @throws ElementException
	 */
	public static void setTags(ActionElementData actionData, Pod pod) throws ElementException {
		Set<Tag> tags = pod.getTags();
		String tagsString = "";
		for (Tag tag : tags) {
			tagsString = tag.getName() + ", ";
		}
		// remove the last comma
		tagsString = tagsString.substring(0, tagsString.length() - 2);
		// set the fieldsets into element data
		setElementData(actionData, "tags", tagsString);
	}

	public static void populateElementData(ActionElementData actionData, List<Customer> list) throws AudiumException {
		setElementData(actionData, "customer_count", list.size());
		if (list.size() == 0) {
			return;
		}
		// get first customer
		Customer customer = list.get(0);
		populateElementData(actionData, customer);
	}
	
	public static void populateElementData(ActionElementData actionData, Customer customer) throws AudiumException {
		setElementData(actionData, "customer_id", customer.getCustomerId());
		setElementData(actionData, "customer_ref_url", customer.getRefURL());

		setFieldSets(actionData, customer);
		
		setDataElements(actionData, customer);
	}

	public static void populateElementData(ActionElementData actionData, Pod pod) throws AudiumException {
		setElementData(actionData, "pod_id", pod.getPodId());
		setElementData(actionData, "media_type", pod.getMediaType());
		setElementData(actionData, "customer_id", pod.getCustomerId());
		setElementData(actionData, "pod_ref_url", pod.getRefURL());
		setElementData(actionData, "request_id", pod.getRequestId());

		setFieldSets(actionData, pod);

		setTags(actionData, pod);

		setDataElements(actionData, pod);
	}

	/**
	 * Adds the string values of key and value to the element as element data
	 * @param actionData the CVP actionData for the element
	 * @param key the name of the data
	 * @param value the value of the data
	 * @throws ElementException
	 */
	public static void setElementData(ActionElementData actionData, Object key, Object value) throws ElementException {
		String k = String.valueOf(key);
		String v = String.valueOf(value);
		actionData.addToLog("Setting DataElements Data on element", k + " = " + v);
		actionData.setElementData(k, v);
	}

	/**
	 * Wrapper method for setting all the DataElements from a Context Service Customer as key-value pairs in the element data of the element
	 * @param actionData the CVP actionData for the element
	 * @param customer the Context Service Customer object to get the data elements from
	 * @throws ElementException
	 */
	public static void setDataElements(ActionElementData actionData, Customer customer) throws ElementException {
		// iterate over the data elements and assign those key-value pairs to this element's data
		Set<DataElement> dataElements = customer.getDataElements();
		setDataElements(actionData, dataElements);
	}
	
	/**
	 * Wrapper method for setting all the DataElements from a Context Service POD as key-value pairs in the element data of the element
	 * @param actionData the CVP actionData for the element
	 * @param pod the Context Service POD object to get the data elements from
	 * @throws ElementException
	 */
	public static void setDataElements(ActionElementData actionData, Pod pod) throws ElementException {
		// iterate over the data elements and assign those key-value pairs to this element's data
		Set<DataElement> dataElements = pod.getDataElements();
		setDataElements(actionData, dataElements);
	}

	public static void setDataElements(ActionElementData actionData, Set<DataElement> dataElements) throws ElementException {
		for (DataElement de : dataElements) {
			setElementData(actionData, de.getDataKey(), de.getDataValue());
		}
	}

	public static void setFieldSets(ActionElementData actionData, Pod pod) throws ElementException {
		List<String> fieldsets = pod.getFieldsets();
		setFieldSets(actionData, fieldsets);
	}

	public static void setFieldSets(ActionElementData actionData, Customer customer) throws ElementException {
		List<String> fieldsets = customer.getFieldsets();
		setFieldSets(actionData, fieldsets);
	}

	public static void setFieldSets(ActionElementData actionData, List<String> fieldsets) throws ElementException {
		String fieldsetString = "";
		for (String fieldset : fieldsets) {
			fieldsetString = fieldset + ", ";
		}
		// remove the last comma
		fieldsetString = fieldsetString.substring(0, fieldsetString.length() - 2);
		// set the fieldsets into element data
		setElementData(actionData, "fielsets", fieldsetString);
	}

	public static ContextServiceClient getContextServiceClient() throws AudiumException {
		ContextServiceClient contextServiceClient = ContextServiceClientManager.getInstance().getContextServiceClient();
		if (!ContextServiceClientManager.getInstance().isRegistered()) {
			throw new AudiumException("Context Service object initialization failed.");
		}
		return contextServiceClient;
	}

	/**
	 * @param msgName the message name to send to trace
	 * @param msgBody the message body of text to send to trace
	 */
	public static void trace(String msgName, String msgBody) {
		SvcMgrUtil.getSvcMgr().trace(msgName, msgBody);
	}

	/**
	 * @return true if service manager log trace is on
	 */
	public static boolean traceEnabled() {
		return SvcMgrUtil.getSvcMgr().willTrace(1L);
	}

	public static SearchParameters extractSearchParameters(String queryParameters) {
		Map<String, String> map = extractParametersMap(queryParameters);
		return new SearchParameters(map);
	}

	public static HashMap<String, Object> extracteDataElements(String dataElementsString) {
		return new HashMap<String, Object>(CxdUtil.extractParametersMap(dataElementsString));
	}

	public static Map<String, String> extractParametersMap(String queryParameters) {
		Map<String, String> parametersMap = new HashMap<String, String>();

		int NAME = 0;
		int VALUE = 1;

		for (String nameValueEntry : NameValueTableUtil.splitByComma(queryParameters)) {
			if (!StringUtils.isEmpty(nameValueEntry)) {
				String name = NameValueTableUtil.getProcessedText(nameValueEntry, NAME);
				String value = NameValueTableUtil.getProcessedText(nameValueEntry, VALUE);
				parametersMap.put(name, value);
			}
		}
		return parametersMap;
	}

	public static List<String> extractFieldSetList(String fieldsets) {
		String[] array = fieldsets.split(",");
		List<String> list = new ArrayList<String>(array.length);
		for (String item : array) {
			list.add(item.trim());
		}
		return list;
	}

	public static Set<Tag> extractTagList(String tagsString) {
		String[] array = tagsString.split(",");
		Set<Tag> set = new HashSet<Tag>();
		for (String item : array) {
			set.add(new Tag(item.trim()));
		}
		return set;
	}

}
