package com.cisco.cxdemo.cvp.cs;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import com.audium.server.action.contextservice.ContextService;
import com.audium.server.session.ActionElementData;
import com.audium.server.voiceElement.ActionElementBase;
import com.audium.server.voiceElement.AudiumElement;
import com.audium.server.voiceElement.ElementData;
import com.audium.server.voiceElement.ElementException;
import com.audium.server.voiceElement.Setting;
import com.cisco.cxdemo.cvp.cs.util.CxdUtil;
import com.cisco.thunderhead.DataElement;
import com.cisco.thunderhead.client.ClientResponse;
import com.cisco.thunderhead.client.ContextServiceClient;
import com.cisco.thunderhead.errors.ApiException;
import com.cisco.thunderhead.pod.Pod;
import com.cisco.thunderhead.tag.Tag;
import com.cisco.thunderhead.util.DataElementUtils;

/**
 * This element cannot be renamed to CxdUpdatePodAction - Call Studio and CVP will not load it, for reasons I could not determine or reason.
 * @author Coty Condry
 *
 */
public class CxdUpdatePodAction extends ActionElementBase implements AudiumElement, ContextService {
	private static final String ELEMENT_NAME = "CXD_POD_Update";
	private static final String ELEMENT_DESCRIPTION = "Update a Context Service POD.";
	
	private static final String POD_ID = "podId";
	private static final String CUSTOMER_ID = "customerId";
	private static final String TAGS = "tags";
	private static final String REPLACE_TAGS = "replaceTags";
	private static final String FIELDSETS = "fieldsets";
	private static final String REPLACE_FIELDSETS = "replaceFieldsets";
	private static final String MEDIA_TYPE = "mediaType";
	private static final String DATA_ELEMENTS = "dataElements";

	private static final String POD_ID_DISPLAY_NAME = "POD ID";
	private static final String CUSTOMER_ID_DISPLAY_NAME = "Customer ID";
	private static final String TAGS_DISPLAY_NAME = "Tags";
	private static final String REPLACE_TAGS_DISPLAY_NAME = "Replace Tags";
	private static final String FIELDSETS_DISPLAY_NAME = "Fieldsets";
	private static final String REPLACE_FIELDSETS_DISPLAY_NAME = "Replace Fieldsets";
	private static final String MEDIA_TYPE_DISPLAY_NAME = "Media Type";
	private static final String DATA_ELEMENTS_DISPLAY_NAME = "Data Elements";
	
	private static final String POD_ID_TOOLTIP = "Required - POD ID of POD you want to update.";
	private static final String CUSTOMER_ID_TOOLTIP = "Optional - The new Customer ID to assign this POD to.";
	private static final String TAGS_TOOLTIP = "Optional - A comma-separated list of tags to be associated with the POD.";
	private static final String REPLACE_TAGS_TOOLTIP = "If true, replaces the current tags on the POD with the tags listed above. If false, add the new tags onto the POD's current list of tags.";
	private static final String FIELDSETS_TOOLTIP = "Optional - A comma-separated list of field sets.";
	private static final String REPLACE_FIELDSETS_TOOLTIP = "If true, replaces the current fieldsets on the POD with the tags listed above. If false, add the new fieldsets onto the POD's current list of fieldsets.";
	private static final String MEDIA_TYPE_TOOLTIP = "Optional - The new media type for the POD.";
	private static final String DATA_ELEMENTS_TOOLTIP = "Optional - data elements to add or update data on the POD. All fields must belong to the cisco.base.pod fieldset or one of the fieldsets specified in the fieldsets setting of this element.";
	
	private static final String TIMEOUT_RETRIES_TOOLTIP = "The number of times to retry the request when Context Service returns a timeout error in ApiException. Defaults to 1 retry.";
	private static final String TIMEOUT_RETRIES_DISPLAY_NAME = "Timeout Retries";
	private static final String TIMEOUT_RETRIES = "timeoutRetries";
	
	public String getElementName() {
		return ELEMENT_NAME;
	}

	public String getDisplayFolderName() {
		return "Cisco\\CXDemo\\Context Service";
	}

	public String getDescription() {
		return ELEMENT_DESCRIPTION;
	}

	public Setting[] getSettings() throws ElementException {
		Setting[] settingArray = new Setting[9];
		// POD UUID text input
		settingArray[0] = new Setting(POD_ID, POD_ID_DISPLAY_NAME, POD_ID_TOOLTIP, true, true, true, Setting.STRING);
		// Customer UUID text input
		settingArray[1] = new Setting(CUSTOMER_ID, CUSTOMER_ID_DISPLAY_NAME, CUSTOMER_ID_TOOLTIP, false, true, true, Setting.STRING);
		// tags - text input
		settingArray[2] = new Setting(TAGS, TAGS_DISPLAY_NAME, TAGS_TOOLTIP, false, true, true, Setting.STRING);
		// replace tags - boolean input
		settingArray[3] = new Setting(REPLACE_TAGS, REPLACE_TAGS_DISPLAY_NAME, REPLACE_TAGS_TOOLTIP, false, true, false, Setting.BOOLEAN);
		// fieldsets - text input
		settingArray[4] = new Setting(FIELDSETS, FIELDSETS_DISPLAY_NAME, FIELDSETS_TOOLTIP, false, true, true, Setting.STRING);
		// replace fieldsets - boolean input
		settingArray[5] = new Setting(REPLACE_FIELDSETS, REPLACE_FIELDSETS_DISPLAY_NAME, REPLACE_FIELDSETS_TOOLTIP, false, true, false, Setting.BOOLEAN);
		// mediaType - dropdown selection
		settingArray[6] = new Setting(MEDIA_TYPE, MEDIA_TYPE_DISPLAY_NAME, MEDIA_TYPE_TOOLTIP, false, true, false, new String[] {
				" ", 
				"voice", 
				"video", 
				"chat", 
				"email", 
				"social", 
				"mobile", 
				"web", 
				"alert", 
		});
		settingArray[6].setDefaultValue(" ");
		// dataElements - key-value pairs
		settingArray[7] = new Setting(DATA_ELEMENTS, DATA_ELEMENTS_DISPLAY_NAME, DATA_ELEMENTS_TOOLTIP, false, true, true, Setting.NAME_VALUE_TABLE);// timeout retries - Integer input
		settingArray[8] = new Setting(TIMEOUT_RETRIES, TIMEOUT_RETRIES_DISPLAY_NAME, TIMEOUT_RETRIES_TOOLTIP, true, true, true, Setting.INT);
		settingArray[8].setDefaultValue("1");
		return settingArray;
	}

	public ElementData[] getElementData() throws ElementException {
		return new ElementData[] { 
				new ElementData("pod_id", "The UUID of the updated POD."),
				new ElementData("pod_ref_url", "The Context Service reference URL for this POD."),
				new ElementData("customer_id", "The UUID of the updated POD's customer."),
				new ElementData("customer_ref_url", "The Context Service reference URL for this POD's customer."),
				new ElementData("media_type", "The media type of the POD."),
				new ElementData("status", "Whether the Context Service HTTP call succeeded or failed. Returns 'success' or 'failed'"),
				new ElementData("status_code", "The HTTP status code returned by Context Service."),
				new ElementData("fieldsets", "A comma-separated list of the Context Service fieldsets this POD uses."),
				new ElementData("tags", "A comma-separated list of the Context Service tags this POD has.")
		};
	}

	public void doAction(String name, ActionElementData actionData) throws Exception {
		// get the setting values for this element
		String podId = actionData.getActionElementConfig().getSettingValue(POD_ID, actionData);
		String customerId = actionData.getActionElementConfig().getSettingValue(CUSTOMER_ID, actionData);
		String tagsString = actionData.getActionElementConfig().getSettingValue(TAGS, actionData);
		boolean replaceTags = actionData.getActionElementConfig().getBooleanSettingValue(REPLACE_TAGS, actionData);
		String fieldsetsString = actionData.getActionElementConfig().getSettingValue(FIELDSETS, actionData);
		boolean replaceFieldsets = actionData.getActionElementConfig().getBooleanSettingValue(REPLACE_FIELDSETS, actionData);
		String mediaType = actionData.getActionElementConfig().getSettingValue(MEDIA_TYPE, actionData);
		String dataElementsString = actionData.getActionElementConfig().getSettingValue(DATA_ELEMENTS, actionData);
		int timeoutRetries = actionData.getActionElementConfig().getIntSettingValue(TIMEOUT_RETRIES, actionData);
		
		// get context service client
		ContextServiceClient contextServiceClient = CxdUtil.getContextServiceClient();
		
		
		Pod pod = null;
		// retry on timeout errors
		for (int i = 0; i <= timeoutRetries; i++) {
			try {
				// start the action length timer
				long beforeTime = System.currentTimeMillis();
				// perform action
				pod = contextServiceClient.get(Pod.class, podId);
				// stop timer
				long afterTime = System.currentTimeMillis();
				// calculate timer
				Integer time = Integer.valueOf((int)(afterTime - beforeTime));
				// log timer
				actionData.addToLog("Get CS POD - time taken", time.toString());
				// exit retry loop because we're done
				break;
			} catch (ApiException e) {
				if (e.getMessage().contains("timeout")) {
					// retry on timeout
					continue;
				} else {
					// rethrow other errors
					throw e;
				}
			}
		}
		
		// update customer ID, if set
		if (!customerId.isEmpty()) {
			pod.setCustomerId(UUID.fromString(customerId));
		}
		// update Media Type, if set to a non-blank value selection
		if (!mediaType.trim().isEmpty()) {
			pod.setMediaType(mediaType);
		}
		// update tags, if set
		if (!tagsString.isEmpty()) {
			Set<Tag> tags = CxdUtil.extractTagList(tagsString);
			if (replaceTags) {
				pod.setTags(tags);
			} else {
				// append tags
				Set<Tag> currentTags = pod.getTags();
				currentTags.addAll(tags);
				pod.setTags(currentTags);
			}
		}
		// update fieldsets, if set
		if (!fieldsetsString.isEmpty()) {
			List<String> fieldsets = CxdUtil.extractFieldSetList(fieldsetsString);
			if (replaceFieldsets) {
				pod.setFieldsets(fieldsets);
			} else {
				// append fieldsets
				List<String> currentFieldsets = pod.getFieldsets();
				currentFieldsets.addAll(fieldsets);
				pod.setFieldsets(currentFieldsets);
			}
		}
		// update dataElements, if set
		if (!dataElementsString.isEmpty()) {
			// extract map from the element settings value
			Map<String, Object> dataElements = CxdUtil.extracteDataElements(dataElementsString);
			Set<DataElement> dataElementsMap = DataElementUtils.convertDataMapToSet(dataElements);
			
			// append fieldsets
			Set<DataElement> currentDataElements = pod.getDataElements();
			currentDataElements.addAll(dataElementsMap);
			pod.setDataElements(currentDataElements);
		}
		// update contributor using this machine's hostname, if available
		pod.setNewContributor(CxdUtil.getMachineContributor(actionData));
		
		
		ClientResponse response = null;
		// retry on timeout errors
		for (int i = 0; i <= timeoutRetries; i++) {
			try {
				// start the action length timer
				long beforeTime = System.currentTimeMillis();
				// perform action
				response = contextServiceClient.update(pod);
				// stop timer
				long afterTime = System.currentTimeMillis();
				// calculate timer
				Integer time = Integer.valueOf((int)(afterTime - beforeTime));
				if (response.getStatus() >= 200 && response.getStatus() < 300) {
					// log success
					actionData.addToLog("Update CS POD - status", "success");
					// log timer
					actionData.addToLog("Update CS POD - time taken", time.toString());
					// set the data on this element
					CxdUtil.setElementData(actionData, "status_code", response.getStatus());
					CxdUtil.setElementData(actionData, "status", "success");
					CxdUtil.populateElementData(actionData, pod);
				} else {
					// log failure
					actionData.addToLog("Update CS POD - status", "failed");
					// log timer
					actionData.addToLog("Update CS POD - time taken", time.toString());
					// set the data on this element
					CxdUtil.setElementData(actionData, "status_code", response.getStatus());
					CxdUtil.setElementData(actionData, "status", "failed");
					// CxdUtil.populateElementData(actionData, pod);
				}
				// exit retry loop because we're done
				break;
			} catch (ApiException e) {
				if (e.getMessage().contains("timeout")) {
					// retry on timeout
					continue;
				} else {
					// rethrow other errors
					throw e;
				}
			}
		}
	}
}
