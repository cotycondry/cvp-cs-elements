package com.cisco.cxdemo.cvp.cs;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.audium.server.action.contextservice.ContextService;
import com.audium.server.session.ActionElementData;
import com.audium.server.voiceElement.ActionElementBase;
import com.audium.server.voiceElement.AudiumElement;
import com.audium.server.voiceElement.ElementData;
import com.audium.server.voiceElement.ElementException;
import com.audium.server.voiceElement.Setting;
import com.cisco.cxdemo.cvp.cs.util.CxdUtil;
import com.cisco.thunderhead.DataElement;
import com.cisco.thunderhead.client.ClientResponse;
import com.cisco.thunderhead.client.ContextServiceClient;
import com.cisco.thunderhead.customer.Customer;
import com.cisco.thunderhead.errors.ApiException;
import com.cisco.thunderhead.util.DataElementUtils;

/**
 * This element updates a Context Service customer. You can update or replace the fieldsets, and/or update data elements.
 * @author Coty Condry
 *
 */
public class CxdUpdateCustomerAction extends ActionElementBase implements AudiumElement, ContextService {
	private static final String ELEMENT_NAME = "CXD_Customer_Update";
	private static final String ELEMENT_DESCRIPTION = "Update a Context Service Customer.";
	
	private static final String CUSTOMER_ID = "customerId";
	private static final String CUSTOMER_ID_DISPLAY_NAME = "Customer ID";
	private static final String CUSTOMER_ID_TOOLTIP = "Required - The UUID of the Context Service Customer you want to update.";
	
	private static final String FIELDSETS = "fieldsets";
	private static final String FIELDSETS_DISPLAY_NAME = "Fieldsets";
	private static final String FIELDSETS_TOOLTIP = "Optional - A comma-separated list of field sets.";
	
	private static final String REPLACE_FIELDSETS = "replaceFieldsets";
	private static final String REPLACE_FIELDSETS_DISPLAY_NAME = "Replace Fieldsets";
	private static final String REPLACE_FIELDSETS_TOOLTIP = "If true, replaces the current fieldsets on the Customer with the tags listed above. If false, add the new fieldsets onto the Customer's current list of fieldsets.";
	
	private static final String DATA_ELEMENTS = "dataElements";
	private static final String DATA_ELEMENTS_DISPLAY_NAME = "Data Elements";
	private static final String DATA_ELEMENTS_TOOLTIP = "Optional - data elements to add or update data on the Customer. All fields must belong to the cisco.base.pod fieldset or one of the fieldsets specified in the fieldsets setting of this element.";
	
	private static final String TIMEOUT_RETRIES = "timeoutRetries";
	private static final String TIMEOUT_RETRIES_DISPLAY_NAME = "Timeout Retries";
	private static final String TIMEOUT_RETRIES_TOOLTIP = "The number of times to retry the request when Context Service returns a timeout error in ApiException. Defaults to 1 retry. Retries are counted separately for the lookup request and update request (each are retried this many times).";
	
	public String getElementName() {
		return ELEMENT_NAME;
	}

	public String getDisplayFolderName() {
		return "Cisco\\CXDemo\\Context Service";
	}

	public String getDescription() {
		return ELEMENT_DESCRIPTION;
	}

	public Setting[] getSettings() throws ElementException {
		Setting[] settingArray = new Setting[5];
		// Customer UUID text input
		settingArray[0] = new Setting(CUSTOMER_ID, CUSTOMER_ID_DISPLAY_NAME, CUSTOMER_ID_TOOLTIP, false, true, true, Setting.STRING);
		// fieldsets - text input
		settingArray[1] = new Setting(FIELDSETS, FIELDSETS_DISPLAY_NAME, FIELDSETS_TOOLTIP, false, true, true, Setting.STRING);
		// replace fieldsets - boolean input
		settingArray[2] = new Setting(REPLACE_FIELDSETS, REPLACE_FIELDSETS_DISPLAY_NAME, REPLACE_FIELDSETS_TOOLTIP, false, true, false, Setting.BOOLEAN);
		// dataElements - key-value pairs
		settingArray[3] = new Setting(DATA_ELEMENTS, DATA_ELEMENTS_DISPLAY_NAME, DATA_ELEMENTS_TOOLTIP, false, true, true, Setting.NAME_VALUE_TABLE);
		// timeout retries - Integer input
		settingArray[4] = new Setting(TIMEOUT_RETRIES, TIMEOUT_RETRIES_DISPLAY_NAME, TIMEOUT_RETRIES_TOOLTIP, true, true, true, Setting.INT);
		settingArray[4].setDefaultValue("1");
		return settingArray;
	}

	public ElementData[] getElementData() throws ElementException {
		return new ElementData[] { 
				new ElementData("customer_id", "The UUID of the updated POD's customer."),
				new ElementData("customer_ref_url", "The Context Service reference URL for this POD's customer."),
				new ElementData("status", "Whether the Context Service HTTP call succeeded or failed. Returns 'success' or 'failed'"),
				new ElementData("status_code", "The HTTP status code returned by Context Service."),
				new ElementData("fieldsets", "A comma-separated list of the Context Service fieldsets this POD uses."),
		};
	}

	public void doAction(String name, ActionElementData actionData) throws Exception {
		// get the setting values for this element
		String customerId = actionData.getActionElementConfig().getSettingValue(CUSTOMER_ID, actionData);
		String fieldsetsString = actionData.getActionElementConfig().getSettingValue(FIELDSETS, actionData);
		boolean replaceFieldsets = actionData.getActionElementConfig().getBooleanSettingValue(REPLACE_FIELDSETS, actionData);
		String dataElementsString = actionData.getActionElementConfig().getSettingValue(DATA_ELEMENTS, actionData);
		int timeoutRetries = actionData.getActionElementConfig().getIntSettingValue(TIMEOUT_RETRIES, actionData);
		
		// get context service client
		ContextServiceClient contextServiceClient = CxdUtil.getContextServiceClient();
		
		
		Customer customer = null;
		// retry on timeout errors
		for (int i = 0; i <= timeoutRetries; i++) {
			try {
				// start the action length timer
				long beforeTime = System.currentTimeMillis();
				// perform action
				customer = contextServiceClient.get(Customer.class, customerId);
				// stop timer
				long afterTime = System.currentTimeMillis();
				// calculate timer
				Integer time = Integer.valueOf((int)(afterTime - beforeTime));
				// log timer
				actionData.addToLog("Get CS Customer - time taken", time.toString());
				// exit retry loop because we're done
				break;
			} catch (ApiException e) {
				if (e.getMessage().contains("timeout")) {
					// retry on timeout
					continue;
				} else {
					// rethrow other errors
					throw e;
				}
			}
		}
		
		// update fieldsets, if set
		if (!fieldsetsString.isEmpty()) {
			List<String> fieldsets = CxdUtil.extractFieldSetList(fieldsetsString);
			if (replaceFieldsets) {
				customer.setFieldsets(fieldsets);
			} else {
				// append and merge fieldsets
				List<String> currentFieldsets = customer.getFieldsets();
				currentFieldsets.addAll(fieldsets);
				customer.setFieldsets(currentFieldsets);
			}
		}
		
		// update dataElements, if set
		if (!dataElementsString.isEmpty()) {
			// extract map from the element settings value
			Map<String, Object> dataElements = CxdUtil.extracteDataElements(dataElementsString);
			Set<DataElement> dataElementsMap = DataElementUtils.convertDataMapToSet(dataElements);
			
			// append fieldsets
			Set<DataElement> currentDataElements = customer.getDataElements();
			currentDataElements.addAll(dataElementsMap);
			customer.setDataElements(currentDataElements);
		}
		
		// update contributor using this machine's hostname, if available
		customer.setNewContributor(CxdUtil.getMachineContributor(actionData));
		
		
		ClientResponse response = null;
		// retry on timeout errors
		for (int i = 0; i <= timeoutRetries; i++) {
			try {
				// start the action length timer
				long beforeTime = System.currentTimeMillis();
				// perform action
				response = contextServiceClient.update(customer);
				// stop timer
				long afterTime = System.currentTimeMillis();
				// calculate timer
				Integer time = Integer.valueOf((int)(afterTime - beforeTime));
				if (response.getStatus() >= 200 && response.getStatus() < 300) {
					// log success
					actionData.addToLog("Update CS POD - status", "success");
					// log timer
					actionData.addToLog("Update CS POD - time taken", time.toString());
					// set the data on this element
					CxdUtil.setElementData(actionData, "status_code", response.getStatus());
					CxdUtil.setElementData(actionData, "status", "success");
					CxdUtil.populateElementData(actionData, customer);
				} else {
					// log failure
					actionData.addToLog("Update CS POD - status", "failed");
					// log timer
					actionData.addToLog("Update CS POD - time taken", time.toString());
					// set the data on this element
					CxdUtil.setElementData(actionData, "status_code", response.getStatus());
					CxdUtil.setElementData(actionData, "status", "failed");
				}
				// exit retry loop because we're done
				break;
			} catch (ApiException e) {
				if (e.getMessage().contains("timeout")) {
					// retry on timeout
					continue;
				} else {
					// rethrow other errors
					throw e;
				}
			}
		}
	}
}
