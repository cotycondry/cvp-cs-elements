# cvp-cs-elements
This project is a rewrite of the 4 Cisco-provided Context Service CVP elements to add new functionality and options while remaining drop-in replacements the out-of-the-box Context Service CVP elements. These elements will attach returned Customer and POD information as element data so that it is directly available for use in the rest of the CVP script.

## Compile Requirements
This project was compiled with Java 1.7 because CVP 11.5 runs Java 1.7. You will need to reference the following libraries from the Cisco/Call Studio/Eclipse/plugins/ folder to compile this project:
```
plugins/com.audiumcorp.studio.library.common/lib/ccbu-common.jar
plugins/com.audiumcorp.studio.library.common/lib/cvp-common.jar
plugins/com.audiumcorp.studio.library.common/lib/commons-lang.jar
plugins/com.audiumcorp.studio.library.framework/lib/json-path.jar
plugins/com.audiumcorp.studio.library.framework/lib/framework.jar
plugins/com.audiumcorp.studio.library.framework/lib/servlet-api.jar
plugins/com.audiumcorp.studio.library.framework/lib/context-service-sdk.jar
plugins/com.audiumcorp.studio.elements.core/lib/elements.jar
```

## Runtime Requirements
It requires org.json.jar to be in the classpath for Call Studio / CVP. I used org.json version 20161205, but an earlier one will probably work, too. Upload org.json.jar and cvp-cs-elements.jar to the following folders:

Call Studio (then restart studio)
```
C:\Cisco\CallStudio\eclipse\plugins\com.audiumcorp.studio.library.common\lib
```
VXML Server (then restart VXML Server service)
```
C:\Cisco\CVP\VXMLServer\common\lib
```

Call Studio Debugger:
```
C:\Cisco\CallStudio\eclipse\plugins\com.audiumcorp.studio.debug.runtime\AUDIUM_HOME\common\lib
```

## Usage
Use these elements as drop-in replacements for the Cisco-provided Context Service action elements, and reference the customer and POD data elements information directly from the CVP elements. For example, the CXD_Customer_Lookup element will store all the customer object information directly on the element so you could use the following element data references in your script:
- {Data.Element.CXD_Customer_Lookup_01.customer_count}
- {Data.Element.CXD_Customer_Lookup_01.customer_id}
- {Data.Element.CXD_Customer_Lookup_01.customer_ref_url}
- {Data.Element.CXD_Customer_Lookup_01.fieldsets}
- {Data.Element.CXD_Customer_Lookup_01.Context_First_Name}
- {Data.Element.CXD_Customer_Lookup_01.Context_Last_Name}
- {Data.Element.CXD_Customer_Lookup_01.Context_Customer_External_ID}
- {Data.Element.CXD_Customer_Lookup_01.Context_Home_Phone}

Or from CXD_POD_Read, CXD_POD_Add, CXD_POD_Update:
- {Data.Element.CXD_Customer_Lookup_01.pod_id}
- {Data.Element.CXD_Customer_Lookup_01.pod_ref_url}
- {Data.Element.CXD_Customer_Lookup_01.customer_id}
- {Data.Element.CXD_Customer_Lookup_01.customer_ref_url}
- {Data.Element.CXD_Customer_Lookup_01.fieldsets}
- {Data.Element.CXD_Customer_Lookup_01.tags}
- {Data.Element.CXD_Customer_Lookup_01.media_type}
- {Data.Element.CXD_Customer_Lookup_01.response_json}
- {Data.Element.CXD_Customer_Lookup_01.Context_Notes}
- {Data.Element.CXD_Customer_Lookup_01.Context_POD_Activity_Link}

CXD_POD_Read is also modified to allow you to choose which POD to use to set the element data, defaulting to 0 (the first POD returned).
